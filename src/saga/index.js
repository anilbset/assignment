
import employeeSaga from './employeeSaga';

function* rootSaga(){
    yield employeeSaga()
}

export default rootSaga;