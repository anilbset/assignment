import BASE_URL from '../config';

import { FETCH_USERS, SUBMIT_LOGIN } from '../utils/const'

export const fetchUsersApi = (params) => {
    return fetch(BASE_URL + FETCH_USERS, {
        method: "GET",
        mode: "cors", 
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
    }).then((response) => response.json()).catch((err)=> console.error(err));
}

export const submitLoginApi = (params) => {
    return fetch(BASE_URL + SUBMIT_LOGIN, {
        method: "POST",
        mode: "cors", 
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        body:JSON.stringify(params)
    }).then((response) => response.json()).catch((err)=> console.error(err));
}

