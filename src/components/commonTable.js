import React from 'react';
import PropTypes from 'prop-types'

const Table = (props) => {
    console.log('props in table',props);
    
    return(
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Gender</th>
                    <th>Age</th>
                </tr>
            </thead>
            <tbody>
                {props.data.map((user, index) => (
                    <tr key={index}>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>{user.phoneNo}</td>
                        <td>{user.gender}</td>
                        <td>{user.age}</td>
                    </tr>)
                )}
            </tbody>
        </table>
    )
}


Table.propTypes = {
    data: PropTypes.array.isRequired,
}
export default Table;