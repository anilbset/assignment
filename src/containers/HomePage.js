import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CommonTable from '../components/commonTable';

import {connect} from 'react-redux';
import './homepage.css'; 
import Loader from '../shareds/loader';

class HomePage extends Component {
	constructor() {
		super();
		this.state = {
			list: [],
		}
	}

	componentDidMount() {
	}

	componentWillMount() {
		this.props.getUsers();
	}

	render() {
		console.log('list', this.props);
		return (
			<div>
				<Loader showLoader={this.props.showLoader} />
				{/* Header part of Home page */}
				<div>
					<header className="header">
						<span className="app">Assignment App</span>
						<Link to='/login' className="login">Login</Link>
					</header>
				</div>

				<CommonTable
					data={this.props.usersList}
				/>
			</div>
		)

	}
}

const mapStateToProps = (state) => ({usersList: state.homePageStore.usersList});


const mapDispatchToProps = (dispatch) => ({
	getUsers: () => {
		dispatch({
			type: "FETCH_USERS"
		})
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);

