import React, { Component } from 'react';
import {connect } from 'react-redux';
import Loader from '../shareds/loader';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            userObj: {
                userID: '',
                password: '',
            },
            errorObj:{}    
        }
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        // Validation
        const {userObj} = this.state;
        const { userID, password} = userObj;
        let errorObj = {};
        let passRegex =/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
        let emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

        if (!passRegex.test(password)) {
            errorObj.passwordError = "Please enter password with at least 1 Upper case, 1 lower case, 1 special and 1 digits with lenght of 8"; 
        }
 
        if (!emailRegex.test(userID)) { 
            errorObj.emailError = "Please enter valid email";  
        }

        if(Object.keys(errorObj).length){
            this.setState({errorObj})
            return false;
        }

        this.props.submitLogin(userObj);
        //submit login
        
    }

    render() {
        const {userObj} = this.state;
        const {errorObj} = this.state;
        return (
            <div>
                <h2><u>Login Here</u></h2>
                <Loader showLoader={this.props.showLoader} />
                <div className="row">
                    <div className="col-md-4"></div>
                    <form className="col-md-4">
                        <div>
                            <label> User ID :</label> <br></br>
                            <input
                                type="text"
                                name="userID"
                                value={userObj.userID}
                                autoComplete="false"
                                onChange={(event) => {
                                    userObj.userID = event.target.value;
                                    this.setState({userObj});
                                }}
                            />
                            {errorObj.emailError && errorObj.emailError}
                        </div>

                        <div>
                            <label> Password : </label> <br></br>
                            <input type="password" 
                                value={userObj.password} 
                                autoComplete="false"
                                onChange={(event) => {
                                    userObj.password = event.target.value;
                                    this.setState({ password: event.target.value })}
                                } 
                            />
                            {errorObj.passwordError && errorObj.passwordError }
                        </div> <br></br>
                        <button onClick={this.onSubmit}>LOGIN</button>
                    </form>
                </div >
            </div>
        )
    }
}

const mapStateToProps = (state) => ({showLoader: state.loginPageStore.showLoader});


const mapDispatchToProps = (dispatch) => ({
	submitLogin: (userObj) => {
		dispatch({
            type: "SUBMIT_LOGIN",
            payload: userObj
		})
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);