import React, { Component } from 'react';
import './loader.css';

class Loader extends Component {
    render() {
        const showLoader = this.props.showLoader;
        return (
            <div  style={{ display: showLoader ? 'block' : 'none' }}>
                <div className="loader">
                    <div></div>
                    <div></div>
                </div>
            </div>
        )
    }
}

export default Loader;